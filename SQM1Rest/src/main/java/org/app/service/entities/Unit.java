package org.app.service.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Acceptare")
@XmlAccessorType(value= XmlAccessType.NONE)
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Unit implements Serializable{
	
	@Id @GeneratedValue
	private Integer IdUnit;
	private String Nume;
	private String PassRatio;
	private String Acoperire;
	private String Cerinte;
	private String ParametriiIntarre;
	private String ParametriiIesire;
	
	@ManyToOne
	private PlanTestare PlanTestare;
	
	@OneToMany(mappedBy = "Unit")
	private List<CazTest> caztest = new ArrayList<CazTest>();
	@XmlElement
	public Integer getIdUnit() {
		return IdUnit;
	}
	public void setIdUnit(Integer idUnit) {
		IdUnit = idUnit;
	}
	@XmlElement
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	@XmlElement
	public String getPassRatio() {
		return PassRatio;
	}
	public void setPassRatio(String passRatio) {
		PassRatio = passRatio;
	}
	@XmlElement
	public String getAcoperire() {
		return Acoperire;
	}
	public void setAcoperire(String acoperire) {
		Acoperire = acoperire;
	}
	@XmlElement
	public String getCerinte() {
		return Cerinte;
	}
	public void setCerinte(String cerinte) {
		Cerinte = cerinte;
	}
	@XmlElement
	public String getParametriiIntarre() {
		return ParametriiIntarre;
	}
	public void setParametriiIntarre(String parametriiIntarre) {
		ParametriiIntarre = parametriiIntarre;
	}
	@XmlElement
	public String getParametriiIesire() {
		return ParametriiIesire;
	}
	public void setParametriiIesire(String parametriiIesire) {
		ParametriiIesire = parametriiIesire;
	}
	public Unit(Integer idUnit, String nume, String passRatio, String acoperire, String cerinte, String parametriiIntarre,
			String parametriiIesire) {
		super();
		IdUnit = idUnit;
		Nume = nume;
		PassRatio = passRatio;
		Acoperire = acoperire;
		Cerinte = cerinte;
		ParametriiIntarre = parametriiIntarre;
		ParametriiIesire = parametriiIesire;
	}
	public Unit() {
		super();
	}
	@Override
	public String toString() {
		return "Unit [IdUnit=" + IdUnit + ", Nume=" + Nume + ", PassRatio=" + PassRatio + ", Acoperire=" + Acoperire
				+ ", Cerinte=" + Cerinte + ", ParametriiIntarre=" + ParametriiIntarre + ", ParametriiIesire="
				+ ParametriiIesire + ", PlanTestare=" + PlanTestare + ", caztest=" + caztest + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdUnit == null) ? 0 : IdUnit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unit other = (Unit) obj;
		if (IdUnit == null) {
			if (other.IdUnit != null)
				return false;
		} else if (!IdUnit.equals(other.IdUnit))
			return false;
		return true;
	}
	
	public void setLink(AtomLink Link){}

	

}
