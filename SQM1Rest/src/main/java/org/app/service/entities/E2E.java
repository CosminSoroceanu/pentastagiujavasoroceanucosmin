package org.app.service.entities;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="E2E")
@XmlAccessorType(value= XmlAccessType.NONE)
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class E2E implements Serializable{
	@Id @GeneratedValue( strategy= AUTO)
	private Integer IdE2e;
	private String Nume;
	private String PassRatio;
	private String Acoperire;
	private String Cerinte;
	private String ParametriiIntarre;
	private String ParametriiIesire;
	
	@ManyToOne
	private PlanTestare planTestare;
	
	@OneToMany(mappedBy = "E2E", cascade =CascadeType.ALL)
	private List<CazTest> caztest = new ArrayList<CazTest>();

	
	public E2E(Integer idE2e, String nume, String passRatio, String acoperire, String cerinte, String parametriiIntarre,
			String parametriiIesire, PlanTestare planTestare, List<CazTest> caztest) {
		super();
		IdE2e = idE2e;
		Nume = nume;
		PassRatio = passRatio;
		Acoperire = acoperire;
		Cerinte = cerinte;
		ParametriiIntarre = parametriiIntarre;
		ParametriiIesire = parametriiIesire;
		this.planTestare = planTestare;
		this.caztest = caztest;
	}
	
	@XmlElement
	public Integer getIdE2e() {
		return IdE2e;
	}

	public void setIdE2e(Integer idE2e) {
		IdE2e = idE2e;
	}
	@XmlElement
	public String getNume() {
		return Nume;
	}

	public void setNume(String nume) {
		Nume = nume;
	}
	@XmlElement
	public String getPassRatio() {
		return PassRatio;
	}

	public void setPassRatio(String passRatio) {
		PassRatio = passRatio;
	}
	@XmlElement
	public String getAcoperire() {
		return Acoperire;
	}

	public void setAcoperire(String acoperire) {
		Acoperire = acoperire;
	}
	@XmlElement
	public String getCerinte() {
		return Cerinte;
	}

	public void setCerinte(String cerinte) {
		Cerinte = cerinte;
	}
	@XmlElement
	public String getParametriiIntarre() {
		return ParametriiIntarre;
	}

	public void setParametriiIntarre(String parametriiIntarre) {
		ParametriiIntarre = parametriiIntarre;
	}
	@XmlElement
	public String getParametriiIesire() {
		return ParametriiIesire;
	}

	public void setParametriiIesire(String parametriiIesire) {
		ParametriiIesire = parametriiIesire;
	}
	@XmlElement
	public PlanTestare getPlanTestare() {
		return planTestare;
	}

	public void setPlanTestare(PlanTestare planTestare) {
		this.planTestare = planTestare;
	}

	public List<CazTest> getCaztest() {
		return caztest;
	}

	public void setCaztest(List<CazTest> caztest) {
		this.caztest = caztest;
	}

	public E2E() {
		super();
	}
	public E2E(Integer idE2e, String nume, String passRatio, String acoperire, String cerinte, String parametriiIntarre,
			String parametriiIesire, PlanTestare planTestare) {
		super();
		IdE2e = idE2e;
		Nume = nume;
		PassRatio = passRatio;
		Acoperire = acoperire;
		Cerinte = cerinte;
		ParametriiIntarre = parametriiIntarre;
		ParametriiIesire = parametriiIesire;
		this.planTestare = planTestare;
	}
	
	public E2E(int i, String string1, String string2, String string3,String string4,String string5,String string6, PlanTestare plantestare7) {
		// TODO Auto-generated constructor stub
	}

	

}
