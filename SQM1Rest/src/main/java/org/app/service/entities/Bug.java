package org.app.service.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Bug implements Serializable {
	
	@Id @GeneratedValue
	private Integer IdBug;
	private String Descriere;
	private String Status;
	private String Prioritate;
	private String Persoana;
	
	@ManyToOne
	private CazTest CazTest;
	
	
	public Integer getIdBug() {
		return IdBug;
	}
	public void setIdBug(Integer idBug) {
		IdBug = idBug;
	}
	public String getDescriere() {
		return Descriere;
	}
	public void setDescriere(String descriere) {
		Descriere = descriere;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getPrioritate() {
		return Prioritate;
	}
	public void setPrioritate(String prioritate) {
		Prioritate = prioritate;
	}
	public String getPersoana() {
		return Persoana;
	}
	public void setPersoana(String persoana) {
		Persoana = persoana;
	}
	public Bug(Integer idBug, String descriere, String status, String prioritate, String persoana) {
		super();
		IdBug = idBug;
		Descriere = descriere;
		Status = status;
		Prioritate = prioritate;
		Persoana = persoana;
	}
	public Bug() {
		super();
	}
	@Override
	public String toString() {
		return "Bug [IdBug=" + IdBug + ", Descriere=" + Descriere + ", Status=" + Status + ", Prioritate=" + Prioritate
				+ ", Persoana=" + Persoana + ", CazTest=" + CazTest + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdBug == null) ? 0 : IdBug.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bug other = (Bug) obj;
		if (IdBug == null) {
			if (other.IdBug != null)
				return false;
		} else if (!IdBug.equals(other.IdBug))
			return false;
		return true;
	}
	
	
	
	

}
