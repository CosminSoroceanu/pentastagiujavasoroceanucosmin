package org.app.service.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static javax.persistence.GenerationType.AUTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CazTest")
@XmlAccessorType(value= XmlAccessType.NONE)
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class CazTest implements Serializable {
	
	@Id @GeneratedValue ( strategy= AUTO)
	private Integer IdTest;
	private String Titlu;
	private String Descriere;
	private String RezultatAsteptat;
	private String RezultatActual;
	private String Comentarii;
	
	@OneToMany(mappedBy = "CazTest" , cascade =CascadeType.ALL)
	private List<Bug> bug = new ArrayList<Bug>();
	
	@ManyToOne
	private Module Module;
	
	@ManyToOne
	private Acceptare Acceptare;
	
	@ManyToOne
	private E2E E2E;
	
	@ManyToOne
	private Unit Unit;
	
	
	
	@XmlElement
	public Integer getIdTest() {
		return IdTest;
	}
	public void setIdTest(Integer idTest) {
		IdTest = idTest;
	}
	@XmlElement
	public String getTitlu() {
		return Titlu;
	}
	public void setTitlu(String titlu) {
		Titlu = titlu;
	}
	@XmlElement
	public String getDescriere() {
		return Descriere;
	}
	public void setDescriere(String descriere) {
		Descriere = descriere;
	}
	@XmlElement
	public String getRezultatAsteptat() {
		return RezultatAsteptat;
	}
	public void setRezultatAsteptat(String rezultatAsteptat) {
		RezultatAsteptat = rezultatAsteptat;
	}
	@XmlElement
	public String getRezultatActual() {
		return RezultatActual;
	}
	public void setRezultatActual(String rezultatActual) {
		RezultatActual = rezultatActual;
	}
	@XmlElement
	public String getComentarii() {
		return Comentarii;
	}
	public void setComentarii(String comentarii) {
		Comentarii = comentarii;
	}
	@XmlElementWrapper (name = "bug") @XmlElement( name = "Bug")
	public List<Bug> getBug() {
		return bug;
	}
	public void setBug(List<Bug> bug) {
		this.bug = bug;
	}
	@XmlElement
	public Module getModule() {
		return Module;
	}
	public void setModule(Module module) {
		Module = module;
	}
	@XmlElement
	public Acceptare getAcceptare() {
		return Acceptare;
	}
	public void setAcceptare(Acceptare acceptare) {
		Acceptare = acceptare;
	}
	@XmlElement
	public E2E getE2E() {
		return E2E;
	}
	public void setE2E(E2E e2e) {
		E2E = e2e;
	}
	@XmlElement
	public Unit getUnit() {
		return Unit;
	}
	public void setUnit(Unit unit) {
		Unit = unit;
	}
	
	
	public CazTest(Integer idTest, String titlu, String descriere, String rezultatAsteptat, String rezultatActual,
			String comentarii) {
		super();
		IdTest = idTest;
		Titlu = titlu;
		Descriere = descriere;
		RezultatAsteptat = rezultatAsteptat;
		RezultatActual = rezultatActual;
		Comentarii = comentarii;
	}
	public CazTest() {
		super();
	}
	@Override
	public String toString() {
		return "CazTest [IdTest=" + IdTest + ", Titlu=" + Titlu + ", Descriere=" + Descriere + ", RezultatAsteptat="
				+ RezultatAsteptat + ", RezultatActual=" + RezultatActual + ", Comentarii=" + Comentarii + ", bug="
				+ bug + ", Module=" + Module + ", Acceptare=" + Acceptare + ", E2E=" + E2E + ", Unit=" + Unit
				+  "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdTest == null) ? 0 : IdTest.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CazTest other = (CazTest) obj;
		if (IdTest == null) {
			if (other.IdTest != null)
				return false;
		} else if (!IdTest.equals(other.IdTest))
			return false;
		return true;
	}


	public static String BASE_URL = "http://localhost:8080/SQM1/data/CazTest/";
	@XmlElement(name= "link")
	public AtomLink getLink() throws Exception{
		String restUrl = BASE_URL + this.getTitlu();
		return new AtomLink(restUrl, "get-CazTest");
	}
	
	public void setLink(AtomLink Link){}
}
