package org.app.service.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Module implements Serializable {
	
	@Id @GeneratedValue
	private Integer IdModul;
	private String Cerinte;
	private String EchipamentTestare;
	private String Nume;
	private String Acoperire;
	private String ParametriiIntarre;
	private String ParametriiIesire;
	private String PassRatio;
	
	@ManyToOne
	private PlanTestare PlanTestare;
	
	@OneToMany(mappedBy = "Module")
	private List<CazTest> caztest = new ArrayList<CazTest>();
	
	public Integer getIdModul() {
		return IdModul;
	}
	public void setIdModul(Integer idModul) {
		IdModul = idModul;
	}
	public String getCerinte() {
		return Cerinte;
	}
	public void setCerinte(String cerinte) {
		Cerinte = cerinte;
	}
	public String getEchipamentTestare() {
		return EchipamentTestare;
	}
	public void setEchipamentTestare(String echipamentTestare) {
		EchipamentTestare = echipamentTestare;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public String getAcoperire() {
		return Acoperire;
	}
	public void setAcoperire(String acoperire) {
		Acoperire = acoperire;
	}
	public String getParametriiIntarre() {
		return ParametriiIntarre;
	}
	public void setParametriiIntarre(String parametriiIntarre) {
		ParametriiIntarre = parametriiIntarre;
	}
	public String getParametriiIesire() {
		return ParametriiIesire;
	}
	public void setParametriiIesire(String parametriiIesire) {
		ParametriiIesire = parametriiIesire;
	}
	public String getPassRatio() {
		return PassRatio;
	}
	public void setPassRatio(String passRatio) {
		PassRatio = passRatio;
	}
	public Module(Integer idModul, String cerinte, String echipamentTestare, String nume, String acoperire,
			String parametriiIntarre, String parametriiIesire, String passRatio) {
		super();
		IdModul = idModul;
		Cerinte = cerinte;
		EchipamentTestare = echipamentTestare;
		Nume = nume;
		Acoperire = acoperire;
		ParametriiIntarre = parametriiIntarre;
		ParametriiIesire = parametriiIesire;
		PassRatio = passRatio;
	}
	public Module() {
		super();
	}
	@Override
	public String toString() {
		return "Module [IdModul=" + IdModul + ", Cerinte=" + Cerinte + ", EchipamentTestare=" + EchipamentTestare
				+ ", Nume=" + Nume + ", Acoperire=" + Acoperire + ", ParametriiIntarre=" + ParametriiIntarre
				+ ", ParametriiIesire=" + ParametriiIesire + ", PassRatio=" + PassRatio + ", PlanTestare=" + PlanTestare
				+ ", caztest=" + caztest + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdModul == null) ? 0 : IdModul.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Module other = (Module) obj;
		if (IdModul == null) {
			if (other.IdModul != null)
				return false;
		} else if (!IdModul.equals(other.IdModul))
			return false;
		return true;
	}
	public PlanTestare getPlanTestare() {
		return PlanTestare;
	}
	public void setPlanTestare(PlanTestare planTestare) {
		PlanTestare = planTestare;
	}
	public Module(org.app.service.entities.PlanTestare planTestare) {
		super();
		PlanTestare = planTestare;
	}
	
	
	
	

}
