package org.app.service.entities;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="PlanTestare")
@XmlAccessorType(value= XmlAccessType.NONE)
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class PlanTestare implements Serializable {
	@Id @GeneratedValue ( strategy= AUTO)
	private Integer IdPlanTest;
	private String Responsabil;
	private String Descriere;	
	private String NumeProiect;
	private String DataInceput;
	
	@OneToMany(mappedBy = "PlanTestare", cascade =CascadeType.ALL)
	private List<Module> Modul = new ArrayList<Module>();
	
	@OneToMany(mappedBy = "PlanTestare", cascade =CascadeType.ALL)
	private List<Acceptare> Accept = new ArrayList<Acceptare>();
	
	@OneToMany(mappedBy = "planTestare", cascade =CascadeType.ALL)
	private List<E2E> e2e = new ArrayList<E2E>();
	
	@OneToMany(mappedBy = "PlanTestare", cascade =CascadeType.ALL)
	private List<Unit> unit = new ArrayList<Unit>();
	
	@Override
	public String toString() {
		return "PlanTestare [IdPlanTest=" + IdPlanTest + ", Responsabil=" + Responsabil + ", Descriere=" + Descriere
				+ ", NumeProiect=" + NumeProiect + ", DataInceput=" + DataInceput + ", Modul=" + Modul + ", Accept="
				+ Accept + ", e2e=" + e2e + ", unit=" + unit + "]";
	}
	
	
	@XmlElement
	public Integer getIdPlanTest() {
		return IdPlanTest;
	}
	public void setIdPlanTest(Integer idPlanTest) {
		IdPlanTest = idPlanTest;
	}
	@XmlElement
	public String getResponsabil() {
		return Responsabil;
	}
	public void setResponsabil(String responsabil) {
		Responsabil = responsabil;
	}
	@XmlElement
	public String getDescriere() {
		return Descriere;
	}
	public void setDescriere(String descriere) {
		Descriere = descriere;
	}
	@XmlElement
	public String getNumeProiect() {
		return NumeProiect;
	}
	public void setNumeProiect(String numeProiect) {
		NumeProiect = numeProiect;
	}
	@XmlElement
	public String getDataInceput() {
		return DataInceput;
	}
	public void setDataInceput(String dataInceput) {
		DataInceput = dataInceput;
	}
	@XmlElementWrapper (name = "Modul") @XmlElement( name = "Module")
	public List<Module> getModul() {
		return Modul;
	}
	public void setModul(List<Module> modul) {
		Modul = modul;
	}
	@XmlElementWrapper (name = "Accept") @XmlElement( name = "Acceptare")
	public List<Acceptare> getAccept() {
		return Accept;
	}
	public void setAccept(List<Acceptare> accept) {
		Accept = accept;
	}
	@XmlElementWrapper (name = "e2e") @XmlElement( name = "E2E")
	public List<E2E> getE2e() {
		return e2e;
	}
	public void setE2e(List<E2E> e2e) {
		this.e2e = e2e;
	}
	@XmlElementWrapper (name = "unit") @XmlElement( name = "Unit")
	public List<Unit> getUnit() {
		return unit;
	}
	public void setUnit(List<Unit> unit) {
		this.unit = unit;
	}

	
	
	public PlanTestare(Integer idPlanTest, String responsabil, String descriere, String numeProiect, String dataInceput) {
		super();
		IdPlanTest = idPlanTest;
		Responsabil = responsabil;
		Descriere = descriere;
		NumeProiect = numeProiect;
		DataInceput = dataInceput;
	}
	public PlanTestare() {
		super();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdPlanTest == null) ? 0 : IdPlanTest.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanTestare other = (PlanTestare) obj;
		if (IdPlanTest == null) {
			if (other.IdPlanTest != null)
				return false;
		} else if (!IdPlanTest.equals(other.IdPlanTest))
			return false;
		return true;
	}
	public static String BASE_URL = "http://localhost:8080/SQM1/data/PlanTestare/";
	@XmlElement(name= "link")
	public AtomLink getLink() throws Exception{
		String restUrl = BASE_URL + this.getNumeProiect();
		return new AtomLink(restUrl, "get-PlanTestare");
	}
	
	public void setLink(AtomLink Link){}
}
