package org.app.service.ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Acceptare;
import org.app.service.entities.PlanTestare;
@Path("Acceptare")
@Stateless @LocalBean
public class AcceptareServiceEJB extends EntityRepositoryBase<Acceptare> implements  AcceptareService, Serializable {

private static Logger logger = Logger.getLogger(AcceptareServiceEJB.class.getName());
	
	
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	
	public AcceptareServiceEJB(){
		}
	
	@PostConstruct
	public void init()
	{
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}
	public String getMessage(){return " Acceptare is working";}
	//CRUD
	//CREATE OR UPDATE
	@PUT @Path("/{id}")
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public Acceptare addAcceptare(Acceptare acceptareToAdd){
		em.persist(acceptareToAdd);
		em.flush();
		em.refresh(acceptareToAdd);
		return acceptareToAdd;
	}
	//READ
	@GET @Path("/{id}")
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public Acceptare getAcceptareByAcceptareID(@PathParam("id") Integer AcceptareID){
		logger.info("**** DEBUG REST getAcceptareByAcceptareID(): id = " + AcceptareID);
		return em.find(Acceptare.class, AcceptareID);
	}
	@GET
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	public Collection<Acceptare> getAcceptare(){
		List<Acceptare> Acceptare = em.createQuery("SELECT a FROM Acceptare a", Acceptare.class)
				.getResultList();
		logger.info("**** DEBUG REST Acceptare.size()= " + Acceptare.size());
		return Acceptare;
	}
	//REMOVE
	@DELETE
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public String removeAcceptare(Acceptare AcceptareToDelete){
		AcceptareToDelete = em.merge(AcceptareToDelete);
		em.remove(AcceptareToDelete);
		em.flush();
		return "True";
	}
	@GET @Path("/{Nume}")
	@Produces({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public Acceptare getAcceptareByNume(@PathParam ("Nume") String Nume) {
		return em.createQuery("SELECT a FROM Acceptare a WHERE a.Nume = :Nume", Acceptare.class)
				.setParameter("Nume",  Nume)
				.getSingleResult();
	}
	//OTHERS
	public String sayRest(){
		return "Acceptare Service is On...";
	
}
	}
