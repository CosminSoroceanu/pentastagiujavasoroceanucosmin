package org.app.service.ejb;
import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.CazTest;
@Path("CazTest")
@Stateless @LocalBean
public class CazTestServiceEJB extends EntityRepositoryBase<CazTest> implements CazTestService,Serializable{
	
	private static Logger logger = Logger.getLogger(CazTestServiceEJB.class.getName());
	
	
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	
	public CazTestServiceEJB(){
		}
	
	@PostConstruct
	public void init()
	{
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}
	public String getMessage(){return " CazTest is working";}
	//CRUD
	//CREATE OR UPDATE
	@PUT @Path("/{IdCazTest}")
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public CazTest addCazTest(CazTest caztestToAdd){
		em.persist(caztestToAdd);
		em.flush();
		em.refresh(caztestToAdd);
		return caztestToAdd;
	}
	//READ
	@GET @Path("/{IdCazTest}")
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public CazTest getCazTestByCazTestID(@PathParam("IdCazTest") Integer IdCazTest){
		logger.info("**** DEBUG REST getCazTestByCazTestID(): id = " + IdCazTest);
		return em.find(CazTest.class, IdCazTest);
	}
	@GET
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	public Collection<CazTest> getCazTest(){
		List<CazTest> CazTest = em.createQuery("SELECT c FROM CazTest c", CazTest.class)
				.getResultList();
		logger.info("**** DEBUG REST CazTest.size()= " + CazTest.size());
		return CazTest;
	}
	//REMOVE
	@DELETE
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public String removeCazTest(CazTest CazTestToDelete){
		CazTestToDelete = em.merge(CazTestToDelete);
		em.remove(CazTestToDelete);
		em.flush();
		return "True";
	}
	//Custom READ
	/*@Override
	public CazTest getCazTestByCazTestID(Integer IDCazTest) {
		return em.createQuery("SELECT c FROM CazTest c WHERE c.titlu = :titlu", CazTest.class)
				.setParameter("titlu", IDCazTest)
				.getSingleResult();
	}*/
	//OTHERS
	public String sayRest(){
		return "CazTest Service is On...";
	}

}
