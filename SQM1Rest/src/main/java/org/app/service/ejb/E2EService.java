package org.app.service.ejb;

import java.util.Collection;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.E2E;
import org.app.service.entities.PlanTestare;

@Remote
public interface E2EService extends EntityRepository<E2E> {

	
	E2E addE2E(E2E e2eToAdd);
	
	E2E getE2EByIdPlanTest(Integer IdPlanTest);
	
	Collection<E2E> getE2E();
	
	String removeE2E(E2E e2eToDelete);
	
	String getMessage();
	
	PlanTestare getPlanTestareByIdPlanTest(Integer IdPlanTest);
}
