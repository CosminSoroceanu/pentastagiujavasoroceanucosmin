package org.app.service.ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.PlanTestare;
@Path("PlanTestare")
@Stateless @LocalBean
public class PlanTestareServiceEJB extends EntityRepositoryBase<PlanTestare> implements  PlanTestareService, Serializable {
	
	private static Logger logger = Logger.getLogger(PlanTestareServiceEJB.class.getName());
	
	
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	
	public PlanTestareServiceEJB(){
		}
	
	@PostConstruct
	public void init()
	{
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}
	public String getMessage(){return " PlanTestare is working";}
	//CRUD
	//CREATE OR UPDATE
	
	@PUT @Path("/{id}")
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public PlanTestare addPlanTestare(PlanTestare plantestareToAdd){
		em.persist(plantestareToAdd);
		em.flush();
		em.refresh(plantestareToAdd);
		return plantestareToAdd;
	}
	//READ
	@GET @Path("/{id}")
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public PlanTestare getPlanTestareByPlanTestareID(@PathParam("id") Integer PlanTestareID){
		logger.info("**** DEBUG REST getPlanTestareByPlanTestareID(): id = " + PlanTestareID);
		return em.find(PlanTestare.class, PlanTestareID);
	}
	@GET
	@Produces ({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	public Collection<PlanTestare> getPlanTestare(){
		List<PlanTestare> PlanTestare = em.createQuery("SELECT p FROM PlanTestare p", PlanTestare.class)
				.getResultList();
		logger.info("**** DEBUG REST PlanTestare.size()= " + PlanTestare.size());
		return PlanTestare;
	}
	//REMOVE
	@DELETE
	@Consumes({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public String removePlanTestare(PlanTestare PlanTestareToDelete){
		PlanTestareToDelete = em.merge(PlanTestareToDelete);
		em.remove(PlanTestareToDelete);
		em.flush();
		return "True";
	}
	//Custom READ
	@GET @Path("/{responsabil}")
	@Produces({ MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
	@Override
	public PlanTestare getPlanTestareByResponsabil(@PathParam ("responsabil") String responsabil) {
		return em.createQuery("SELECT p FROM PlanTestare p WHERE p.responsabil = :responsabil", PlanTestare.class)
				.setParameter("responsabil",  responsabil)
				.getSingleResult();
	}
	//OTHERS
	public String sayRest(){
		return "PlanTestare Service is On...";
	}

		
		
	
	
}
