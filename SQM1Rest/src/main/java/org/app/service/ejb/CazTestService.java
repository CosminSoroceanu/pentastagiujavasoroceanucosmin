package org.app.service.ejb;

import java.util.Collection;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.CazTest;

@Remote
public interface CazTestService extends EntityRepository<CazTest> {

	CazTest addCazTest(CazTest caztestToAdd);
	
	CazTest getCazTestByCazTestID(Integer IDCazTest);
	
	Collection<CazTest> getCazTest();
	
	String removeCazTest(CazTest caztestToDelete);
	
	String getMessage();
	
	
	
}
