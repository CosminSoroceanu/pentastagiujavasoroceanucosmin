package org.app.service.ejb;

import java.util.Collection;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Acceptare;
import org.app.service.entities.PlanTestare;

@Remote
public interface AcceptareService extends EntityRepository<Acceptare>{
	
	Acceptare addAcceptare(Acceptare acceptareToAdd);
	
	Acceptare getAcceptareByAcceptareID(Integer IDAcceptare);
	
	Collection<Acceptare> getAcceptare();
	Acceptare getAcceptareByNume(String Nume);
	String removeAcceptare(Acceptare acceptareToDelete);
	
	String getMessage();

}
