package org.app.service.ejb;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.E2E;
import org.app.service.entities.PlanTestare;

@Stateless @LocalBean
public class E2EServiceEJB extends EntityRepositoryBase<E2E> implements  E2EService{

	
private static Logger logger = Logger.getLogger(E2EServiceEJB.class.getName());
	
	
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	
	public E2EServiceEJB(){
		}
	
	@PostConstruct
	public void init()
	{
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}
	public String getMessage(){return " E2E is working";}
	//CRUD
	//CREATE OR UPDATE
	@Override
	public E2E addE2E(E2E e2eToAdd){
		em.persist(e2eToAdd);
		em.flush();
		em.refresh(e2eToAdd);
		return e2eToAdd;
	}
	//READ
	public E2E getE2EByIdPlanTest(Integer IdPlanTest){
		return em.find(E2E.class, IdPlanTest);
	}
	public Collection<E2E> getE2E(){
		List<E2E> E2E = em.createQuery("SELECT e FROM E2E e", E2E.class)
				.getResultList();
		return E2E;
	}
	//REMOVE
	public String removeE2E(E2E E2EToDelete){
		E2EToDelete = em.merge(E2EToDelete);
		em.remove(E2EToDelete);
		em.flush();
		return "True";
	}
	//Custom READ
	/*@Override
	public E2E getE2EByE2EID(Integer IDE2e) {
		return em.createQuery("SELECT e FROM E2E e WHERE e.nume = :nume", E2E.class)
				.setParameter("nume", IDE2e)
				.getSingleResult();
	}*/
	//OTHERS
	public String sayRest(){
		return "E2E Service is On...";
	}	
		@Override
		public PlanTestare getPlanTestareByIdPlanTest(Integer IdPlanTest) {
			// TODO Auto-generated method stub
			//return null;// sa creem un select, un query care sa ia candidatul
			return em.createQuery("SELECT p FROM PlanTestare p WHERE p.IdPlanTest = :IdPlanTest", PlanTestare.class)
			.setParameter("IdPlanTest", IdPlanTest)
			.getSingleResult();
		}
}

