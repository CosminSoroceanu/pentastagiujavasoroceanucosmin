package org.app.service.ejb;

import java.util.Collection;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.PlanTestare;


@Remote
public interface PlanTestareService extends EntityRepository<PlanTestare>{
	
	PlanTestare addPlanTestare(PlanTestare plantestareToAdd);
	
	PlanTestare getPlanTestareByPlanTestareID(Integer IDPlanTest);
	
	Collection<PlanTestare> getPlanTestare();
	
	PlanTestare getPlanTestareByResponsabil(String responsabil);
	
	String removePlanTestare(PlanTestare plantestareToDelete);
	
	String getMessage();
	

}
