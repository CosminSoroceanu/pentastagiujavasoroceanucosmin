package org.app.service.ejb.test;
import org.jboss.arquillian.container.test.api.Deployment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;



import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.PlanTestareService;
import org.app.service.ejb.PlanTestareServiceEJB;
import org.app.service.entities.PlanTestare;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;


@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPlanTestareDataServiceEJBArq {
	
		private static Logger logger = Logger.getLogger(TestPlanTestareDataServiceEJBArq.class.getName());
		@EJB
		private static PlanTestareService service;
		
		@Deployment // Arquilian infrastructure
		public static Archive<?> createDeployment() {
		        return ShrinkWrap
		                .create(WebArchive.class, "SQM-S1-test.war")
		                .addPackage(PlanTestare.class.getPackage())
		                .addClass(PlanTestareService.class)
		                .addClass(PlanTestareServiceEJB.class)
		                .addClass(EntityRepository.class)
		                .addClass(EntityRepositoryBase.class)
		                .addAsResource("META-INF/persistence.xml")
		                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		}
		
		@Test
		public void test1_GetMessage() {
			logger.info("DEBUG: Junit TESTING: testGetMessage ...");
			String response = service.getMessage();
			assertNotNull("Data Service failed!", response);
			logger.info("DEBUG: EJB Response ..." + response);
		}

		@Test
		public void test4_GetPlanTestare() {
			logger.info("DEBUG: Junit TESTING: testGetPlanTestari ...");
			Collection<PlanTestare> PlanTestari= service.getPlanTestare();
			assertTrue("Fail to read PlanTestari!", PlanTestari.size() > 0);
		}

		@Test
		public void test3_AddPlanTestare() {
			logger.info("DEBUG: Junit TESTING: testAddPlanTestare ...");
			
			Integer plantestariToAdd = 1;
			for (int i=1; i <= plantestariToAdd; i++){
				service.add(new PlanTestare(i, "Madalina Ionescu"," Descrierea planului de testare 3 ","Proiect3","23/05/2015" ));
			}
			Collection<PlanTestare> plantestari = service.getPlanTestare();
			assertTrue("Fail to add PlanTestari!", plantestari.size() == plantestariToAdd);
		}

		//@Test
		/*public void test2_DeletePlanTestare() {
			logger.info("DEBUG: Junit TESTING: testDeletePlanTestare ...");
			
			Collection<PlanTestare> plantestari = service.getPlanTestare();
			for (PlanTestare p: plantestari)
				service.remove(p);
			Collection<PlanTestare> PlanTestariAfterDelete = service.getPlanTestare();
			assertTrue("Fail to read PlanTestari!", PlanTestariAfterDelete.size() == 0);
		}	*/
}
	




	
	

