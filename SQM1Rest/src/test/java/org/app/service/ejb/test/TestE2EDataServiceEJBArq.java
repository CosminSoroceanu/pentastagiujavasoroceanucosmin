package org.app.service.ejb.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.E2EService;
import org.app.service.ejb.E2EServiceEJB;
import org.app.service.entities.E2E;
import org.app.service.entities.PlanTestare;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestE2EDataServiceEJBArq {
	
	private static Logger logger = Logger.getLogger(TestE2EDataServiceEJBArq.class.getName());
	@EJB
	private static E2EService service;
	
	@Deployment // Arquilian infrastructure
	public static Archive<?> createDeployment() {
	        return ShrinkWrap
	                .create(WebArchive.class, "SCRUM-S3-test.war")
	                .addPackage(E2E.class.getPackage())
	                .addClass(E2EService.class)
	                .addClass(E2EServiceEJB.class)
	                .addClass(EntityRepository.class)
	                .addClass(EntityRepositoryBase.class)
	                .addAsResource("META-INF/persistence.xml")
	                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: testGetMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}

	@Test
	public void test4_GetE2E() {
		logger.info("DEBUG: Junit TESTING: testGetE2Es ...");
		Collection<E2E> E2Es= service.getE2E();
		assertTrue("Fail to read E2Es!", E2Es.size() > 0);
	}

	@Test
	public void test3_AddE2E() {
		logger.info("DEBUG: Junit TESTING: testAddE2E ...");
		
		Integer E2EsToAdd = 1;
		PlanTestare plantestare= service.getPlanTestareByIdPlanTest(7);
		for (int i=1; i <= E2EsToAdd; i++){
			service.add(new E2E(i, "E2E1"," 53.8 ","63.8","Cerintele testului E2E","Parametrii de intrare al testului E2E 1","Parametrii de iesire al testului E2E 1",plantestare ));
		}
		Collection<E2E> E2Es = service.getE2E();
		assertTrue("Fail to add E2E!", E2Es.size() == E2EsToAdd);
	}

	@Test
	public void test2_DeleteE2E() {
		logger.info("DEBUG: Junit TESTING: testDeleteE2E ...");
		
		Collection<E2E> E2Es = service.getE2E();
		for (E2E e: E2Es)
			service.remove(e);
		Collection<E2E> E2EsAfterDelete = service.getE2E();
		assertTrue("Fail to read E2Es!", E2EsAfterDelete.size() == 0);
	}	

}
