package org.app.service.ejb.test;
import org.jboss.arquillian.container.test.api.Deployment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.service.ejb.CazTestService;
import org.app.service.ejb.CazTestServiceEJB;

import org.app.service.entities.CazTest;
import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCazTestDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestCazTestDataServiceEJBArq.class.getName());
	@EJB
	private static CazTestService service;
	
	@Deployment // Arquilian infrastructure
	public static Archive<?> createDeployment() {
	        return ShrinkWrap
	                .create(WebArchive.class, "SQM1.war")
	                .addPackage(CazTest.class.getPackage())
	                .addClass(CazTestService.class)
	                .addClass(CazTestServiceEJB.class)
	                .addClass(EntityRepository.class)
	                .addClass(EntityRepositoryBase.class)
	                .addAsResource("META-INF/persistence.xml")
	                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: testGetMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}

	@Test
	public void test4_GetCazTest() {
		logger.info("DEBUG: Junit TESTING: testGetCazTeste ...");
		Collection<CazTest> CazTeste= service.getCazTest();
		assertTrue("Fail to read CazTeste!", CazTeste.size() > 0);
	}

	@Test
	public void test3_AddCazTest() {
		logger.info("DEBUG: Junit TESTING: testAddCazTest ...");
		
		Integer caztesteToAdd = 1;
		for (int i=1; i <= caztesteToAdd; i++){
			service.add(new CazTest(i, "CazTest2"," Descrierea cazului de test2", "Test rezolvat2 ","Initial2","Nu exista comentarii2" ));
		}
		Collection<CazTest> cazteste = service.getCazTest();
		assertTrue("Fail to add CazTeste!", cazteste.size() == caztesteToAdd);
	}

	/*@Test
	public void test2_DeleteCazTest() {
		logger.info("DEBUG: Junit TESTING: testDeleteCazTest ...");
		
		Collection<CazTest> cazteste = service.getCazTest();
		for (CazTest c: cazteste)
			service.remove(c);
		Collection<CazTest> CazTesteAfterDelete = service.getCazTest();
		assertTrue("Fail to read CazTeste!", CazTesteAfterDelete.size() == 0);
	}	*/

}
