package org.app.service.ejb.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.AcceptareService;
import org.app.service.ejb.AcceptareServiceEJB;
import org.app.service.entities.Acceptare;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAcceptareDataServiceEJBArq {
	
	private static Logger logger = Logger.getLogger(TestAcceptareDataServiceEJBArq.class.getName());
	@EJB
	private static AcceptareService service;
	
	@Deployment // Arquilian infrastructure
	public static Archive<?> createDeployment() {
	        return ShrinkWrap
	                .create(WebArchive.class, "SCRUM-S3-test.war")
	                .addPackage(Acceptare.class.getPackage())
	                .addClass(AcceptareService.class)
	                .addClass(AcceptareServiceEJB.class)
	                .addClass(EntityRepository.class)
	                .addClass(EntityRepositoryBase.class)
	                .addAsResource("META-INF/persistence.xml")
	                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: testGetMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}

	@Test
	public void test4_GetAcceptare() {
		logger.info("DEBUG: Junit TESTING: testGetAcceptari ...");
		Collection<Acceptare> Acceptari= service.getAcceptare();
		assertTrue("Fail to read Acceptari!", Acceptari.size() > 0);
	}

	@Test
	public void test3_AddAcceptare() {
		logger.info("DEBUG: Junit TESTING: testAddAcceptare ...");
		
		Integer acceptariToAdd = 1;
		for (int i=1; i <= acceptariToAdd; i++){
			service.add(new Acceptare(i, "Acceptare3"," 33.6 ","22.8","Cerintele testului3" ));
		}
		Collection<Acceptare> acceptari = service.getAcceptare();
		assertTrue("Fail to add Acceptari!", acceptari.size() == acceptariToAdd);
	}

	//@Test
	/*public void test2_DeleteAcceptare() {
		logger.info("DEBUG: Junit TESTING: testDeleteAcceptare ...");
		
		Collection<Acceptare> acceptari = service.getAcceptare();
		for (Acceptare a: acceptari)
			service.remove(a);
		Collection<Acceptare> AcceptariAfterDelete = service.getAcceptare();
		assertTrue("Fail to read Acceptari!", AcceptariAfterDelete.size() == 0);
	}	*/

}
